// sparsegen is now running sparse to compile file 1test.c
// sparsegen finished compiling file 1test.c
// symbol static_0_0_sfg_version (0x7fe371756ce0), here=1, static=1
// symbol = "sfg_version"
// type   = 5 "int static [signed] [toplevel] sfg_version( ... )"
// modif  = 2014 "static [signed] [toplevel]"
// cg_declare_function for static_0_0_sfg_version returning=0
var static_0_0_sfg_version; // size=0
// generate_function_body():
function static_0_0_sfg_version(fp, stack) {
var sp;
var state = 0;
for (;;) {
switch (state) {
case 0:
sp = 0;
sp = fp + sp;
// .L0x7fe3717b9010 // basic block number 0
//   <entry-point>
//   copy.32     %r2 <- $20
//   dead        %r2
//   ret.32      %r2
// 
// generate_one_insn(): INSN is "<entry-point>"
// generate_one_insn(): INSN is "copy.32     %r2 <- $20"
// hardreg H0 assigned to register class 0
// pseudo %r2 assigned to hardregref [int:H0/1]
H0 = 20;
// generate_one_insn(): INSN is "dead        %r2"
// pseudo %r2 in hardregref [int:H0/1] is dying
// generate_one_insn(): INSN is "ret.32      %r2"
return H0;
// pseudo %r2 has died and is no longer in hardregref [int:H0/1]
// hardreg H0 now unused
// --- in ---
// --- spill ---
// --- out ---
} } }

// symbol _main (0x7fe371757040), here=1, static=0
// symbol = "main"
// type   = 5 "int extern [addressable] [signed] [toplevel] main( ... )"
// modif  = 2118 "extern [addressable] [signed] [toplevel]"
// cg_declare_function for _main returning=0
var _main; // size=0
// hardreg H0 assigned to register class 0
// pseudo %arg1 assigned to hardregref [int:H0/1]
// arg %arg1 in hardregref [int:H0/1]
// hardreg H1 assigned to register class 0
// hardreg H2 assigned to register class 0
// pseudo %arg2 assigned to hardregref [ptr:H2/1+H1/1]
// arg %arg2 in hardregref [ptr:H2/1+H1/1]
// generate_function_body():
function _main(fp, stack, H0, H1, H2) {
var sp;
var state = 0;
for (;;) {
switch (state) {
case 0:
sp = 0;
sp = fp + sp;
// .L0x7fe3717b90e0 // basic block number 2
//   <entry-point>
//   copy.32     %r5 <- sfg_version
//   dead        %r5
//   call.32     %r3 <- %r5
//   copy.32     %r6 <- $0
//   dead        %r6
//   ret.32      %r6
// 
// generate_one_insn(): INSN is "<entry-point>"
// generate_one_insn(): INSN is "copy.32     %r5 <- sfg_version"
// pseudo %r5 assigned to hardregref [fnptr:H0/1]
H0 = static_0_0_sfg_version;
// generate_one_insn(): INSN is "dead        %r5"
// pseudo %r5 in hardregref [fnptr:H0/1] is dying
// generate_one_insn(): INSN is "call.32     %r3 <- %r5"
// pseudo %r3 assigned to hardregref [int:H1/1]
H1 = H0(sp, stack);
// pseudo %r5 has died and is no longer in hardregref [fnptr:H0/1]
// hardreg H0 now unused
// generate_one_insn(): INSN is "copy.32     %r6 <- $0"
// pseudo %r6 assigned to hardregref [int:H0/1]
H0 = 0;
print(H1);
// generate_one_insn(): INSN is "dead        %r6"
// pseudo %r6 in hardregref [int:H0/1] is dying
// generate_one_insn(): INSN is "ret.32      %r6"
return H0;
// pseudo %r6 has died and is no longer in hardregref [int:H0/1]
// hardreg H0 now unused
// --- in ---
// --- spill ---
// --- out ---
} } }

function initializer(// hardreg H0 assigned to register class 0
// hardreg H1 assigned to register class 0
) {
var H0;
var H1;
var state = 0;
for (;;) {
switch (state) {
case 0:
// cg_export for _main
return;
} } }

clue_add_initializer(initializer);
