
/*
 *  Copyright t lefering, David Given
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

var fs = require("fs");
var util = require("util");

function print(s)
{
	util.print(s);
	util.print("\n");
}

function quit()
{
	process.exit();
}

/* Node doesn't have an include() function. And eval() has to be called from
 * the top level or it doesn't work. WTF? */

function load(filename)
{
	return fs.readFileSync(filename, "utf-8");
}

/* Load the Clue crt and libc. */

eval(load("crt.js"));
eval(load("libc.js"));
eval(load("output.js"));

/* Load the Clue compiled program. */

var argv = process.argv;
var argc = 3;
while (argv[argc] != "--")
{
	if (!argv[argc])
	{
		print("Command line did not contain a --\n");
		quit();
	}

	eval(load(argv[argc]));
	argc++;
}

/* And run it. */

{
	var cargs = [];
	
	for (var i = argc+1; i < argv.length; i++)
	{
		var v = clue_newstring(argv[i]);
		cargs.push(0);
		cargs.push(v);
	}

    clue_run_initializers();
    _main(0, [], argv.length - argc, 0, cargs);
    quit();
}
